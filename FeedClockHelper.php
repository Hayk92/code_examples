<?php

namespace App\Console;


use Carbon\Carbon;
use TCG\Voyager\Facades\Voyager;

class FeedClockHelper
{
    protected $importer;
    protected $container;
    private $season;

    protected $apiKey = 'API_KEY';
    private $pushAccessLevel = 'production';
    private $language_code = 'en';
    private $pushVersion = 5;
    private $url;
    private $clockUrl;
    private $baseUrl = 'http://api_url';
    private $streamType = [
        "payload-game-clock",
        "payload-event-clock",
        "payload-event-play_clock",
        "payload-event-wall_clock",
        "payload-event-start_situation-clock",
        "payload-event-end_situation-clock",
    ];

    protected $stream;

    public function __construct()
    {
        $this->url = $this->baseUrl."nfl/official/$this->pushAccessLevel/stream/$this->language_code/clock/subscribe?api_key=$this->apiKey";
        $this->season = Voyager::setting('current-seasons.current_season_football');
        $this->importer = \App\Jobs\NflClockFeedImporter::getInstance();
    }

    public function init(){
        $games = \App\NFLGame::query()->select('id')
            ->where('scheduled', '<=', \Carbon\Carbon::now())
            ->where('season', $this->season)
            ->where(function ($query){
                $query->where('status', '<>', 'closed');
            })->first();

        if (is_null($games) && $this->pushAccessLevel === 'simulation'){
            $games = (object)['id' => "00fdfbf6-62af-4336-b7ef-54482a0bd028"];
        }

        if (!is_null($games)){
            $this->streaming($games, function () use($games){
                if (!$this->importer->ran){
                    $this->importer->handlePushClock($games->id, $this->stream);
                }
            });
        }else{
            flush();
            sleep((60 * 20));
            fclose($this->stream);
            $this->importer->close_dispatch = false;
        }
    }

    private function streaming($games, $runStreaming){
        if (!$this->importer->close_dispatch) {
            $this->importer->close_dispatch = true;
            $this->stream = fopen($this->url, 'r');
        }
        $runStreaming();
    }
}