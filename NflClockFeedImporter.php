<?php

namespace App\Jobs;

use App\Parser\SportradarFeedParser;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use JsonStreamingParser\Listener\GeoJsonListener;
use TCG\Voyager\Facades\Voyager;

class NflClockFeedImporter implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    private static $instance;
    public $ran = false;
    public $listener = false;
    private $season;
    private $current_game;
    public $close_dispatch = false;
    public $stream;
    private $streamType = [
        "payload-game-clock",
        "payload-event-clock",
        "payload-event-play_clock",
        "payload-event-wall_clock",
        "payload-event-start_situation-clock",
        "payload-event-end_situation-clock",
    ];

    public static function getInstance(){
        if (is_null(self::$instance)){
            self::$instance = new NflClockFeedImporter();
        }
        return self::$instance;
    }
    /**
     * Create a new job instance.
     *
     * @return void
     */
    private function __construct()
    {
        $this->season = Voyager::setting('current-seasons.current_season_football');
    }

    // cron method alias
    public function handlePushClock($game, $stream)
    {
        //Closing this stream for ticket T13115476

        $this->ran = true;
        $this->current_game = $game;
        $this->stream = $stream;

        //handleThe clock
        $this->ran = !$this->handlePushClockStream( 'clock', $this->streamType );
    }

    /**
     * Execute the job.
     *
     * @return boolean
     */
    public function handlePushClockStream($streamType,  $skipFields = [])
    {
        if (is_resource($this->stream) && $stream = $this->stream){
            try {
                while (($buffer = fgets($stream, 4096)) !== false){
                    $obj = json_decode($buffer, true);
                    if (!isset($obj['heartbeat'])) {
                        if (isset($obj['payload'])){
                            $status = isset($obj['metadata']) && isset($obj['metadata']['status'])?$obj['metadata']['status']:'inprogress';
                            dispatch(
                                new GameOriginalActionImporter(
                                    $obj['payload'], $status
                                )
                            );
                        }
                    }
                }
            } catch (\Exception $e) {

            } finally {
                return true;
            }
        }else{
            return true;
        }
    }
}
